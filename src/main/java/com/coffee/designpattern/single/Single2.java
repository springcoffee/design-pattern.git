package com.coffee.designpattern.single;

/**
 * 有问题的写法2
 * 
 * @author coffee<br/>
 *         2018年1月18日下午12:51:04
 */
public class Single2 {
	private static Single2 instance;

	private Single2() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static Single2 getInstance() {
		if (instance == null) {
			System.out.println(Thread.currentThread().getName() + "判断 == null");
			synchronized (Single2.class) {
				System.out.println(Thread.currentThread().getName() + " 开始加锁");
				instance = new Single2();
				System.out.println(Thread.currentThread().getName() + " 释放锁");
			}
		}
		return instance;
	}

	public static void main(String[] args) {
		new Thread("线程1") {
			public void run() {
				System.out.println(Single2.getInstance());
			};
		}.start();

		new Thread("线程2") {
			public void run() {
				System.out.println(Single2.getInstance());
			};
		}.start();
	}
}
