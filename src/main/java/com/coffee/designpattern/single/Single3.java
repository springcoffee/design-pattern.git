package com.coffee.designpattern.single;

/**
 * 不太好的写法3, 虽然没有bug 但是每次getInstance的时候都需要加锁判断
 * 
 * @author coffee<br/>
 *         2018年1月18日下午12:53:23
 */
public class Single3 {
	private static Single3 instance;

	private Single3() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static Single3 getInstance() {
		synchronized (Single3.class) {
			System.out.println(Thread.currentThread().getName() + "判断 == null");
			if (instance == null) {
				System.out.println(Thread.currentThread().getName() + " 开始加锁");
				instance = new Single3();
				System.out.println(Thread.currentThread().getName() + " 释放锁");
			}
		}
		return instance;
	}

	public static void main(String[] args) {
		new Thread("线程1") {
			public void run() {
				System.out.println(Single3.getInstance());
			};
		}.start();

		new Thread("线程2") {
			public void run() {
				System.out.println(Single3.getInstance());
			};
		}.start();
	}
}
