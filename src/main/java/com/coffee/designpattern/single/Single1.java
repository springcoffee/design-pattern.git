package com.coffee.designpattern.single;

/**
 * 有问题的写法1
 * 
 * @author coffee<br/>
 *         2018年1月18日下午12:51:04
 */
public class Single1 {
	private static Single1 instance;

	/**
	 * 
	 */
	private Single1() {
		try {
			// 模拟耗时操作
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static Single1 getInstance() {
		if (instance == null) {
			// 如果这个操作比较耗时, 则会出现问题
			instance = new Single1();
		}
		return instance;
	}

	public static void main(String[] args) {
		new Thread("线程1") {
			public void run() {
				System.out.println(Single1.getInstance());
			};
		}.start();

		new Thread("线程2") {
			public void run() {
				System.out.println(Single1.getInstance());
			};
		}.start();
	}
}
