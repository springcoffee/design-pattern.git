package com.coffee.designpattern.future;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Logger;

/**
 * 讲述future的简单使用
 * 
 * @author coffee
 *
 */
public class Future1 {

	private static Logger logger = Logger.getLogger(Future1.class.getSimpleName());

	public static void main(String[] args) {
		ExecutorService executor = Executors.newCachedThreadPool();
		Callable<String> callable = new Callable<String>() {

			@Override
			public String call() throws Exception {
				logger.info("****** 開始執行");
				Thread.sleep(1000 * 3);
				return "hello world";
			}
		};

		logger.info("准备");
		Future<String> future = executor.submit(callable);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		try {
			logger.info("就绪, 开始获取返回结果");
			String result = future.get();
			logger.info(result);
			logger.info("进行第二次查询");
			result = future.get();
			logger.info(result);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
